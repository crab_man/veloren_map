let image;
let container;

function updateFromHash() {
	const mapIndex = !!window.location.hash ? +window.location.hash.slice(1) : NaN;


	if (!isNaN(mapIndex)) {
		image?.setAttribute("src", `${mapIndex}.png`);
		container?.classList.add("visible");
	} else {
		container?.classList.remove("visible");
		image?.setAttribute("src", `none`);
	}
}

function hide() {
	const url = new URL(window.location);
	url.hash = "";
	window.history.pushState("", "", url)
	updateFromHash();
}

window.addEventListener("hashchange", updateFromHash);
window.addEventListener("DOMContentLoaded", () => {
	image = document.querySelector(".map-overlay img");
	container = document.querySelector(".map-overlay");

	updateFromHash();
	container.addEventListener("click", () => {
		hide();
	});
	document.addEventListener("keydown", (e) => {
		if (e.key == "Escape") {
			hide();
		}
	})
});
